[
  {
    "@context": "http://schema.org",
    "@type": "WebSite",
    "name": "Nina Carducci - Photographie",
    "url": "https://ninacarducci.github.io/",
    "description": "Site web de la photographe Nina Carducci"
  },
  {
    "@context": "http://schema.org",
    "@type": "AboutPage",
    "name": "À propos de moi",
    "description": "Informations sur Nina Carducci, photographe"
  },
  {
    "@context": "http://schema.org",
    "@type": "Photograph",
    "name": "Portfolio de Nina Carducci",
    "description": "Collection de photographies de concerts, entreprises, mariages, portraits, etc."
  },
  {
    "@context": "http://schema.org",
    "@type": "Service",
    "name": "Services de photographie de Nina Carducci",
    "description": "Services incluant shooting photo, retouches, album photos"
  },
  {
    "@context": "http://schema.org",
    "@type": "ContactPage",
    "name": "Page de contact de Nina Carducci",
    "description": "Formulaire de contact pour poser des questions ou demander un devis"
  },
{
  "@context": "http://schema.org",
  "@type": "Organization",
  "name": "Nina Carducci - Photographie",
  "url": "https://ninacarducci.github.io/",
  "description": "Site web de la photographe Nina Carducci",
  "address": {
    "@type": "PostalAddress",
    "streetAddress": "68 avenue Alsace-Lorraine",
    "addressLocality": "Bordeaux",
    "postalCode": "33200"
  },
  "contactPoint": {
    "@type": "ContactPoint",
    "telephone": "05 56 67 78 89",
    "contactType": "customer service",
    "availableLanguage": "French",
    "hoursAvailable": [
      "Monday-Friday 10:00-19:00"
    ]
  }
}
]
